<?php

namespace event\event_20140630_photo_upload;

class Comment extends \Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';
    protected $fillable = array('comment', 'state');
    protected $validateRules = array(
        'comment' => 'required',
    );

    public function photo() {
        return $this->belongsTo('\event\event_20140630_photo_upload\Photo');
    }

}
