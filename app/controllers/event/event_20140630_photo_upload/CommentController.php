<?php

namespace event\event_20140630_photo_upload;

class CommentController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function addCommentForm($photo_id = 0) {

        $photo = Photo::findOrFail($photo_id);
        $data = array();
        $comment = new Comment();
        $comment->photo()->associate($photo);
        return \View::make('event.event_20140630_photo_upload.comment.add', array('comment' => $comment, 'data' => $data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function addComment() {
        $inputs = \Input::all();
        $photo = Photo::findOrFail(\Input::get('photo_id'));
        $comment = new Comment($inputs);
        $comment->photo()->associate($photo);
        $comment->save();

        return \Redirect::to("event/20140630-photo-upload/list-photo");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function remove($id) {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return \Redirect::to("event/20140630-photo-upload/list-photo");
    }

}
