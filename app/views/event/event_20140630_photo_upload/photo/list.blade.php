@extends('event.event_20140630_photo_upload.layouts.container')

@section('content')
<h1>{{trans('route.list_photo')}}</h1>
<a href="{{\URL::to('event/20140630-photo-upload/add-photo')}}">{{trans('route.add_photo')}}</a><br>

@foreach ($data['photo_list'] as $photo)
@if($photo->img_ver)
<a href="{{\URL::to('event/20140630-photo-upload/remove-photo/')}}/{{$photo->id}}">{{trans('route.remove_photo')}}</a><br>
<a href="{{\URL::to($photo->img_ver)}}.jpg" target="_blank"><img src="{{\URL::to($photo->img_ver)}}-tn.jpg" width="320" height="240" /></a><br>
<a href="{{\URL::to('event/20140630-photo-upload/comment-photo')}}/{{$photo->id}}">{{trans('route.comment_photo')}}</a><br>
@endif
<ul>
    @foreach($photo->comments as $comment)
    <li><a href="{{\URL::to('event/20140630-photo-upload/remove-comment')}}/{{$comment->id}}">{{trans('route.remove_comment')}}</a>&nbsp;{{{$comment->comment}}}</li>
    @endforeach
</ul>
@endforeach
{{$data['photo_list']->links()}}
@stop