@extends('event.event_20140630_photo_upload.layouts.container')

@section('content')
<h1>{{trans('route.add_photo')}}</h1>
<a href="{{URL::to('event/20140630-photo-upload/list-photo')}}">{{trans('route.list_photo')}}</a><br>
{{Form::model($photo, array('route' => array('photo.add'), 'files' => true))}}

<div class="form-group">
    {{ Form::file('main-image',null, ['class' => 'form-control']) }}
</div>

<button type="submit" class="btn">{{trans('commons.add')}}</button>

{{ Form::close() }}
@stop