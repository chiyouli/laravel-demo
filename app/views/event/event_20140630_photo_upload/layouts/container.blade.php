<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{trans('commons.front-app-name')}}</title>
    </head>
    <body>
        <div class="container">
            @section('content')
            @show

        </div>
    </body>
</html>