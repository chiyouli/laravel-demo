@extends('event.event_20140630_photo_upload.layouts.container')

@section('content')
<h1>{{trans('route.comment_photo')}}</h1>
<a href="{{URL::to('event/20140630-photo-upload/list-photo')}}">{{trans('route.list_photo')}}</a><br>
{{Form::model($comment, array('route' => array('comment.add', 'photo_id' => $comment->photo->id)))}}
<div class="form-group">
    <a href="{{URL::to($comment->photo->img_ver)}}.jpg" target="_blank"><img src="{{URL::to($comment->photo->img_ver)}}-tn.jpg" width="320" height="240" /></a><br>
</div>
<div class="form-group">
    <label>{{Lang::get('comments.comment')}}</label><br>
    {{ Form::textarea('comment',null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    <button type="submit" class="btn">{{trans('commons.add')}}</button>
</div>
{{ Form::close() }}
@stop