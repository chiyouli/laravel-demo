<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function() {
    return View::make('hello');
});
Route::group(array('prefix' => 'event/20140630-photo-upload'), function() {

    Route::post('/add-photo', array('as' => 'photo.add', 'uses' => 'event\event_20140630_photo_upload\PhotoController@addPhoto'));
    Route::get('/add-photo', array('uses' => 'event\event_20140630_photo_upload\PhotoController@addPhotoForm'));
    Route::get('/remove-photo/{id}', array('uses' => 'event\event_20140630_photo_upload\PhotoController@remove'));
    Route::get('/list-photo', array('uses' => 'event\event_20140630_photo_upload\PhotoController@listPhoto'));
    Route::post('/comment-photo', array('as' => 'comment.add', 'uses' => 'event\event_20140630_photo_upload\CommentController@addComment'));
    Route::get('/comment-photo/{id}', array('uses' => 'event\event_20140630_photo_upload\CommentController@addCommentForm'));
    Route::get('/remove-comment/{id}', array('uses' => 'event\event_20140630_photo_upload\CommentController@remove'));
});
